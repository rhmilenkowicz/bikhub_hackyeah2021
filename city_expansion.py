import numpy as np

# Initializing value of x-axis and y-axis
# in the range -2 to +2
x, y = np.meshgrid(np.linspace(-2, 2, 15), np.linspace(-2, 2, 15))
dst = np.sqrt(x * x + y * y)

# Initializing sigma and muu
sigma = 1
muu = 0.000

# Calculating Gaussian array
gauss = np.exp(-((dst - muu) ** 2 / (2.0 * sigma ** 2)))
gauss2 = np.exp(-((dst - muu) ** 2 / (2.4 * sigma ** 2)))
xddd = np.gradient(gauss2, gauss)
print("2D Gaussian array :\n")
print(gauss)