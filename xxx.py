import pickle

import numpy as np
from keras.layers import Dense, Dropout
# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory
from keras.models import Sequential
from scipy.ndimage import uniform_filter
from sklearn.model_selection import train_test_split

# from DataPreparationAlternate import DataPreparationAlternate
from DataPreparation import DataPreparation
# year = 2016
from HeatMaper import Heatmaper

X = None
Y = None
for year in range(2016,2020):

    with open(f"city_map_{year}_filtered.pkl", "rb") as file:
        city_mapn = pickle.load(file)


    with open(f"city_map_{year+1}_filtered.pkl", "rb") as file:
        city_mapn1 = pickle.load(file)

    #Heatmaper.create_heat_map(city_map=city_mapn,year=year)
    x,y = DataPreparation.create_X_y(city_map_year_n=city_mapn,city_map_year_n_plus_1=city_mapn1)
    if X is None:
        X = x
    else:
        X = np.vstack((X, x))
    if Y is None:
        Y = y
    else:
        Y = np.vstack((Y,y))

from sklearn.preprocessing import StandardScaler
scalerX =  StandardScaler()
X =scalerX.fit_transform(X)


scalerY =  StandardScaler()
Y = scalerY.fit_transform(Y)
x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=123)



model1 = Sequential()
model1.add(Dense(9, input_dim=9, activation='tanh'))
model1.add(Dropout(0.5))
model1.add(Dense(32, activation='tanh'))
model1.add(Dropout(0.5))
model1.add(Dense(32, activation='relu'))
model1.add(Dense(1))
model1.compile(loss='mean_squared_error', optimizer='RMSprop')


model1.fit(x_train, y_train, epochs=20, batch_size=4, shuffle=True)

score = model1.evaluate(x_test,y_test)


##Testing:

with open(f"city_map_{2020}_filtered.pkl", "rb") as file:
    city_map_2020 = pickle.load(file)

x_2020 = DataPreparation.create_X(city_map_year=city_map_2020)
x_2020 = scalerX.transform(x_2020)
y_2021 = model1.predict(x_2020)

y_2020 = scalerY.inverse_transform(y_2021)
y_2021 = np.reshape(y_2021,(11,19))
filtered = uniform_filter(y_2021, size=10, mode='nearest')
city_map_2021 = city_map_2020.astype('float64')
city_map_2021[1:12,1:20] += y_2021

Heatmaper.create_heat_map(city_map_2021,2021)
Heatmaper.create_gradient_heat_map(filtered,2021)
