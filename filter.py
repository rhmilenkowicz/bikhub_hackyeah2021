import pickle

import numpy as np
from scipy.ndimage import uniform_filter

from interpolator import Interpolator

for year in [2016, 2017, 2018, 2019, 2020]:
    with open(f"city_map_{year}.pkl", "rb") as file:
        city_map = pickle.load(file)

    interpolated = Interpolator.interpolate(city_map).astype(np.int32)
    filtered = uniform_filter(interpolated, size=3, mode='nearest')

    with open(f"city_map_{year}_filtered.pkl", "wb") as file:
        pickle.dump(filtered, file)

print("Finished")
