import copy

import numpy as np
from HeatMaper import Heatmaper
from Preprocessor import Preprocessor
import pickle
from DataPreparationAlternate import DataPreparationAlternate
from DataPreparation import DataPreparation
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense ,Dropout,BatchNormalization
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

#year = 2016

X = None
Y = None
for year in range(2016,2020):

    with open(f"city_map_{year}_filtered.pkl", "rb") as file:
        city_mapn = pickle.load(file)


    with open(f"city_map_{year+1}_filtered.pkl", "rb") as file:
        city_mapn1 = pickle.load(file)

    x,y = DataPreparation.create_X_y(city_map_year_n=city_mapn,city_map_year_n_plus_1=city_mapn1)
    if X is None:
        X = x
    else:
        X = np.vstack((X, x))
    if Y is None:
        Y = y
    else:
        Y = np.vstack((Y,y))

from sklearn.preprocessing import MinMaxScaler
scalerX =  MinMaxScaler()
X =scalerX.fit_transform(X)


scalerY =  MinMaxScaler()
Y = scalerY.fit_transform(Y)
x_train, x_test, y_train, y_test = train_test_split(X,
                                                    Y, test_size=0.2, random_state=42)



model1 = Sequential()
model1.add(Dense(9, input_dim=9, activation='tanh'))
model1.add(Dense(128, activation='tanh'))
model1.add(Dense(128, activation='tanh'))
model1.add(Dense(1))
model1.compile(loss='mean_squared_error', optimizer='adam')


#model1.summary()


model1.fit(x_train, y_train, epochs=100, batch_size=10)
score = model1.evaluate(x_test,y_test)

# hehe =model1.predict(x_test)
# hehe = scaler.inverse_transform(hehe)

##Testing:

with open(f"city_map_{2020}_filtered.pkl", "rb") as file:
    city_map_2020 = pickle.load(file)

x_2020 = DataPreparation.create_X(city_map_year=city_map_2020)
x_2020 = scalerX.transform(x_2020)
y_2021 = model1.predict(x_2020)

y_2021 = scalerY.inverse_transform(y_2021)
y_2021 = np.reshape(y_2021,(11,19))
y_2021 = y_2021.astype('float64')
city_map_2020 = city_map_2020.astype('float64')
city_map_2021 = copy.deepcopy(city_map_2020)
city_map_2021[1:12,1:20] = city_map_2021[1:12,1:20] + y_2021
city_map_2021[city_map_2021 < 0 ] = 0


from DataPreparationAlternate import DataPreparationAlternate

y_2021 = DataPreparationAlternate.prepare_gradient_map(y_2021)
y_2021[y_2021 == 0 ] = 1
y_2021 = np.log(y_2021)
#estimator = KerasRegressor(build_fn=model1, epochs=30, batch_size=3, verbose=1)
from HeatMaper import Heatmaper
#y_2021 = scalerY.fit_transform(y_2021)
Heatmaper.create_heat_map(city_map_2021,2021)

