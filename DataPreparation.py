import numpy as np


class DataPreparation:

    @classmethod
    def create_X_y(cls,city_map_year_n,city_map_year_n_plus_1):

        X = np.array([])
        Y = np.array([])
        for x_cord in range(1,city_map_year_n.shape[1]-1):
            for y_cord in range(1,city_map_year_n.shape[0]-1):
                x = city_map_year_n[y_cord-1:y_cord+2,x_cord-1:x_cord+2]
                x = x.flatten()
                y = city_map_year_n_plus_1[y_cord,x_cord] - city_map_year_n[y_cord,x_cord]
                X = np.append(X, x)
                Y = np.append(Y, y)

        X = np.reshape(X,(Y.shape[0],9))
        Y = np.reshape(Y,(Y.shape[0],1))

        return X,Y

    @classmethod
    def create_X(cls,city_map_year):
        X = np.array([])
        for x_cord in range(1, city_map_year.shape[1] - 1):
            for y_cord in range(1, city_map_year.shape[0] - 1):
                x = city_map_year[y_cord - 1:y_cord + 2, x_cord - 1:x_cord + 2]
                x = x.flatten()
                X = np.append(X, x)


        X = np.reshape(X, (209, 9))


        return X

