import numpy as np


class DataPreparationAlternate:

    @classmethod
    def prepare_x(cls, city_map):
        row = -np.zeros((city_map.shape[1]))
        column = -np.transpose(np.zeros((1,city_map.shape[0]+2)))

        new_city_map = np.vstack((city_map,row))
        new_city_map = np.vstack((row, new_city_map))

        new_city_map = np.hstack((column,new_city_map))
        new_city_map = np.hstack((new_city_map,column))
        ## WIP

        return new_city_map

    @classmethod
    def prepare_gradient_map(cls, gradient_map):
        row = np.zeros((gradient_map.shape[1]))
        column = np.transpose(np.zeros((1, gradient_map.shape[0] + 2)))

        new_gradient_map = np.vstack((gradient_map, row))
        new_gradient_map = np.vstack((row, new_gradient_map))

        new_gradient_map = np.hstack((column, new_gradient_map))
        new_gradient_map = np.hstack((new_gradient_map, column))

        return new_gradient_map





