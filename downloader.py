import json
import requests


class BikDownloader:
    CRT = "/home/radoslaw/Downloads/ruda4.crt"
    KEY = "/home/radoslaw/Downloads/ruda4_nopass.key"
    HEADERS = {
        "Cache-Control": "no-cache",
        "Content-type": "application/json",
        "BIK-OAPI-Key": "fa1166eac62743d182566038741708b3",
    }
    YEAR_DECODING = {
        2016: "M16_POPT", 2017: "M17_POPT", 2018: "M18_POPT", 2019: "M19_POPT", 2020: "M20_POPT",
    }

    @classmethod
    def download_data(cls, x, y, size, year):
        json_data = {
            "size": str(size),
            "localization": {
                "utm_x": str(y),  # This is not an error. X and Y are swapped in BIK API!
                "utm_y": str(x)
            },
            "demographicData": cls.YEAR_DECODING[year]
        }
        response = requests.request("POST", 'https://gateway.oapi.bik.pl/bik-api-4/dane-demograficzne-utm',
                                    headers=cls.HEADERS, data=json.dumps(json_data),
                                    cert=(cls.CRT, cls.KEY), verify=False)
        if response.status_code == 200:
            return json.loads(response.content)['demographicData'][cls.YEAR_DECODING[year]]
