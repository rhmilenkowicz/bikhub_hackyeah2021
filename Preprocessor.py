import numpy as np
from sklearn.preprocessing import StandardScaler,MinMaxScaler


class Preprocessor:

    @classmethod
    def scale_data(cls,city_map: np.ndarray):
        shape = city_map.shape
        city_map = city_map.flatten()
        city_map = np.reshape(city_map, (-1,1))
        scaler = MinMaxScaler()
        scaler.fit(city_map)
        scaled_city_map = scaler.transform(city_map)
        scaled_city_map = np.reshape(scaled_city_map,shape)
        return scaled_city_map