import numpy as np
from downloader import BikDownloader
import pickle


class Map:
    """
    Class responsible for storing map. Hardcoded data can be parametrized
    (now is kept simple due to Hackathon time constraints)
    """
    # Grid size
    X = 21
    Y = 13
    # North-West corner coordinates
    BASE_N = 393168
    BASE_E = 5742384
    # Grid deltas to cover the whole city with 1 km x 1 km "squares" (although we are receiving circles)
    DELTA_N = (403250 - BASE_N) / Y
    DELTA_E = (5728599 - BASE_E) / X

    @classmethod
    def fill_map(cls, year):
        city_map = np.empty((cls.Y, cls.X), dtype=object)
        x_true_hits = [0, 5, 10, 15, 20]
        y_true_hits = [0, 3, 6, 9, 12]
        for x in range(cls.X):
            for y in range(cls.Y):
                if x in x_true_hits and y in y_true_hits:
                    city_map[y, x] = BikDownloader.download_data(x=int(cls.BASE_N + y * cls.DELTA_N),
                                                                 y=int(cls.BASE_E + x * cls.DELTA_E),
                                                                 size=500,
                                                                 year=year)
        with open(f"city_map_{year}.pkl", "wb") as file:
            pickle.dump(city_map, file)
        return city_map
