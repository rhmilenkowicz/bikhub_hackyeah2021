import numpy as np
from scipy.interpolate import NearestNDInterpolator


class Interpolator:

    @staticmethod
    def interpolate(city_map):
        grid_x, grid_y = np.mgrid[0:1:21j, 0:1:13j]
        grid_x = np.transpose(grid_x * 20)
        grid_y = np.transpose(grid_y * 12)

        x, y = np.nonzero(city_map)
        coords = list(zip(y, x))
        z = city_map[x, y]

        interp = NearestNDInterpolator(coords, z)
        return interp(grid_x, grid_y)
