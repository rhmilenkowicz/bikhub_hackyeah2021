import numpy as np
import matplotlib.pyplot as plt
from interpolator import Interpolator
import pickle

BASE_N = 393168
BASE_E = 5742384


class Heatmaper:

    # Grid deltas to cover the whole city with 1 km x 1 km "squares" (although we are receiving circles)


    @classmethod
    def create_heat_map(cls,city_map,year):

        # with open(f"city_map_{year}_filtered.pkl", "rb") as file:
        #     city_map = pickle.load(file)

        new_city_map = Interpolator.interpolate(city_map)
        new_city_map = new_city_map.astype('float64')
        new_city_map = (new_city_map)
        heat_map_data = new_city_map


        fig, ax = plt.subplots()
        im = ax.imshow(heat_map_data, cmap='Blues')
        ax.set_xticks(np.arange(21))
        ax.set_yticks(np.arange(13))
        # ... and label them with the respective list entries

        ax.set_xticklabels([round(BASE_E + (5728599 - BASE_E)/21 * i,2)  for i in range(21)])
        ax.set_yticklabels([round(BASE_N + (403250 - BASE_N)/13 * i,2) for i in range(12,-1,-1)])

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=90, ha="center"
                 # ,rotation_mode="anchor"
                 )
        plt.rcParams['xtick.labelsize'] = 'small'
        fig.colorbar(im, ax=ax)
        #
        # Loop over data dimensions and create text annotations.
        # for i in range(13):
        #     for j in range(21):
        #         text = ax.text(j, i, round(heat_map_data[i, j], -2),
        #                        ha="center", va="center", color="black")

        ax.set_title(f"Demographics(population density) year={year}")
        plt.xlabel("Grid's coordinate(UTM[x])")
        plt.ylabel("Grid's coordinate(UTM[y])")

        fig.tight_layout()
        plt.show()

    @classmethod
    def create_gradient_heat_map(cls, city_map, year):
        # with open(f"city_map_{year}_filtered.pkl", "rb") as file:
        #     city_map = pickle.load(file)

        new_city_map = Interpolator.interpolate(city_map)
        new_city_map = new_city_map.astype('float64')
        new_city_map = (new_city_map)
        heat_map_data = new_city_map

        fig, ax = plt.subplots()
        im = ax.imshow(heat_map_data, cmap='RdYlGn')
        ax.set_xticks(np.arange(21))
        ax.set_yticks(np.arange(13))
        # ... and label them with the respective list entries

        ax.set_xticklabels([round(BASE_E + (5728599 - BASE_E) / 21 * i, 2) for i in range(21)])
        ax.set_yticklabels([round(BASE_N + (403250 - BASE_N) / 13 * i, 2) for i in range(12, -1, -1)])

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=90, ha="center"
                 # ,rotation_mode="anchor"
                 )
        plt.rcParams['xtick.labelsize'] = 'small'
        fig.colorbar(im, ax=ax)

        #
        # Loop over data dimensions and create text annotations.
        # for i in range(13):
        #     for j in range(21):
        #         text = ax.text(j, i, round(heat_map_data[i, j], -2),
        #                        ha="center", va="center", color="black")

        ax.set_title(f"Population density change between {year} and {year-1}")
        plt.xlabel("Grid's coordinate(UTM[x])")
        plt.ylabel("Grid's coordinate(UTM[y])")

        fig.tight_layout()
        plt.show()
